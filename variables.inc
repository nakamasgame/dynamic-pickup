#if defined _pickup_variables_included
	#endinput
#else
	#define _pickup_variables_included
#endif

enum E_PICKUPS_DATA {
    epdtSQLID,
    epdtDescription[45],
    epdtModel,
    epdtText[100],
    Float:epdtPossition_x,
    Float:epdtPossition_y,
    Float:epdtPossition_z,
    epdtInt,
    epdtVW,
    Float:epdtInterior_possition_x,
    Float:epdtInterior_possition_y,
    Float:epdtInterior_possition_z,
    epdtInterior_Int,
    epdtInterior_epdtVW,
    epdtCreatorName[MAX_PLAYER_NAME],
    Text3D:epdtText3D,
    epdtPickup
}; 
new PickupsInfo[MAX_DB_PICKUPS][E_PICKUPS_DATA],
    Iterator:PickupIter<MAX_DB_PICKUPS>;

enum E_PICKUP_MODEL {
    epdtDescription[30],
    epdtModel
};

new PickupsModel[][E_PICKUP_MODEL] =
{
    {"Point d'information", 1239},
    {"Maison (verte)", 1273},
    {"Maison (bleu)", 1272},
    {"Maison (Rouge)", 19522},
    {"Maison (Orange)", 19523},
    {"Maison (Jaune)", 19524},
    {"Symbole dollar", 1274},
    {"Coeur", 1240},
    {"Deux silhouettes rouge", 1314},
    {"Icone de montre", 2710},
    {"Icônes vêtement", 1275},
    {"Tête de mort (1)", 1254},
    {"Tête de mort (2)", 1313},
    {"Une Pilule", 1241},
    {"Point d'interrogation", 18631},
    {"Trousse de secours (1)", 11738},
    {"Trousse de secours (2)", 11736},
    {"Etoile de police", 1247},
    {"Gilet par balle", 1242},
    {"TNT", 1654},
    {"bombe", 1252},
    {"Caisse de munition", 19832},
    {"Munition", 2061},
    {"Jerrican", 1650},
    {"Paquet de drogue (Blanc)", 1575}, 
    {"Paquet de drogue (Orange)", 1576}, 
    {"Paquet de drogue (Jaune)", 1577}, 
    {"Paquet de drogue (Vert)", 1578}, 
    {"Paquet de drogue (Bleu)", 1579}, 
    {"Paquet de drogue (Rouge)", 1580}
};