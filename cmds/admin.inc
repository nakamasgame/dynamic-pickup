#if defined _pickup_cmd_admin_included
	#endinput
#else
	#define _pickup_cmd_admin_included
#endif

CMD:pickup(playerid, params[])
{
    MainDialogPickup(playerid);
    return true; 
}

CMD:gotopickup(playerid, params[])
{
    new pickupID;
    if(sscanf(params,"i", pickupID))
        return SendClientMessage(playerid, -1, "/gotopickup [pickupid]");
    if (!Iter_Contains(PickupIter, pickupID))
        return SendClientMessage(playerid, -1, "Aucun pickup ne correspond a l'id saisie.");

    SetPlayerPos(playerid, PickupsInfo[pickupID][epdtPossition_x], PickupsInfo[pickupID][epdtPossition_y], PickupsInfo[pickupID][epdtPossition_z]);
    SetPlayerInterior(playerid, PickupsInfo[pickupID][epdtInt]);
    SetPlayerVirtualWorld(playerid, PickupsInfo[pickupID][epdtVW]);
    return true; 
}


CMD:tp(playerid, params[])
{
    SetPlayerPos(playerid, 246.375991,109.245994,1003.218750);
    SetPlayerInterior(playerid, 10);
    return true; 
}

CMD:debug(playerid, params[])
{
    printf("%d %d", GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
    return true; 
}

CMD:test(playerid, params[])
{
    SetPlayerVirtualWorld(playerid, 300);
    return true; 
}