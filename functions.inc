#if defined _pickup_functions_included
	#endinput
#else
	#define _pickup_functions_included
#endif

forward OnPickupsLoad();
public OnPickupsLoad()
{
    new rows = cache_num_rows();

    for(new r=0; r < rows; ++r) {
        new id = Iter_Alloc(PickupIter);
        cache_get_value_int(r, "id", PickupsInfo[id][epdtSQLID]);
        cache_get_value(r, "description", PickupsInfo[id][epdtDescription], 45);
        cache_get_value_int(r, "model", PickupsInfo[id][epdtModel]);
        cache_get_value(r, "label", PickupsInfo[id][epdtText], 100);
        cache_get_value_float(r, "exterior_x", PickupsInfo[id][epdtPossition_x]);
        cache_get_value_float(r, "exterior_y", PickupsInfo[id][epdtPossition_y]);
        cache_get_value_float(r, "exterior_z", PickupsInfo[id][epdtPossition_z]);
        cache_get_value_int(r, "exterior_vw", PickupsInfo[id][epdtVW]);
        cache_get_value_int(r, "exterior_int", PickupsInfo[id][epdtInt]);
        cache_get_value_float(r, "teleport_x", PickupsInfo[id][epdtInterior_possition_x]);
        cache_get_value_float(r, "teleport_y", PickupsInfo[id][epdtInterior_possition_y]);
        cache_get_value_float(r, "teleport_z", PickupsInfo[id][epdtInterior_possition_z]);
        cache_get_value_int(r, "teleport_vw", PickupsInfo[id][epdtInterior_epdtVW]);
        cache_get_value_int(r, "teleport_int", PickupsInfo[id][epdtInterior_Int]);

        cache_get_value(r, "username", PickupsInfo[id][epdtCreatorName], MAX_PLAYER_NAME);
        StreamerUpdatePickups(id);
    }

    printf("%d pickups has just been loaded.", rows);
    return true;
}

stock InsertPickup(i)
{
    new query[500];
    mysql_format(MySQL, query, sizeof query, "INSERT INTO `gtrp_pickups` (`admin_accounts_id`, `description`, `label`, `model`, `exterior_x`, `exterior_y`, `exterior_z`, `exterior_int`, `exterior_vw`,  `teleport_x`, `teleport_y`, `teleport_z`, `teleport_vw`, `teleport_int`) VALUES ('1', \"%s\", \"%s\", '%d', '%f', '%f', '%f', '%d', '%d', '%f', '%f', '%f', '%d', '%d')", 
        PickupsInfo[i][epdtDescription],
        PickupsInfo[i][epdtText],
        PickupsInfo[i][epdtModel], 
        PickupsInfo[i][epdtPossition_x], 
        PickupsInfo[i][epdtPossition_y],
        PickupsInfo[i][epdtPossition_z],
        PickupsInfo[i][epdtInt], 
        PickupsInfo[i][epdtVW],
        PickupsInfo[i][epdtInterior_possition_x], 
        PickupsInfo[i][epdtInterior_possition_y],
        PickupsInfo[i][epdtInterior_possition_z],
        PickupsInfo[i][epdtInterior_epdtVW], 
        PickupsInfo[i][epdtInterior_Int]);
    mysql_tquery(MySQL, query, "OnInsertPickup", "d", i);
}

forward OnInsertPickup(id);
public OnInsertPickup(id)
{
    PickupsInfo[id][epdtSQLID] = cache_insert_id();
    //ReloadPickup(id);
    StreamerUpdatePickups(id);
}

stock ResetPickup(id)
{
    PickupsInfo[id][epdtSQLID] = 0; 
    PickupsInfo[id][epdtModel] = 1239;
    PickupsInfo[id][epdtPossition_x] = 0.0;
    PickupsInfo[id][epdtPossition_y] = 0.0;
    PickupsInfo[id][epdtPossition_z] = 0.0;
    PickupsInfo[id][epdtInt] = 0;
    PickupsInfo[id][epdtVW] = 0;
    PickupsInfo[id][epdtInterior_possition_x] = 0.0;
    PickupsInfo[id][epdtInterior_possition_y] = 0.0;
    PickupsInfo[id][epdtInterior_possition_z] = 0.0;
    PickupsInfo[id][epdtInterior_epdtVW] = 0;
    PickupsInfo[id][epdtInterior_Int] = 0;
    format(PickupsInfo[id][epdtText] , 100, "");
    format(PickupsInfo[id][epdtDescription], 45, "");
}

stock ReloadPickup(id)
{
    new query[120], sqlID = PickupsInfo[id][epdtSQLID];
    ResetPickup(id);
    Iter_Remove(PickupIter, id);
    StreamerUpdatePickups(id);
    mysql_format(MySQL, query, sizeof query, "SELECT p.*, a.username  FROM gtrp_pickups p INNER JOIN gtrp_accounts a ON p.admin_accounts_id = a.id WHERE p.id = %d", sqlID); 
    mysql_tquery(MySQL, query, "OnPickupsLoad");
}

stock DeletePickup(pickupid)
{
    new query[45]; 
    mysql_format(MySQL, query, sizeof query, "DELETE FROM `gtrp_pickups` WHERE `id`='%d'", PickupsInfo[pickupid][epdtSQLID]);
    mysql_tquery(MySQL, query);

    ResetPickup(pickupid);
    Iter_Remove(PickupIter, pickupid);
    StreamerUpdatePickups(pickupid);
}

stock UpdatePickup(i)
{
    new query[400]; 

    mysql_format(MySQL, query, sizeof query, "UPDATE `gtrp_pickups` SET \
        `description`=\"%s\", \
        `label`='%s', \
        `model`='%d', \
        `exterior_x`='%f', \
        `exterior_y`='%f', \
        `exterior_z`='%f', \
        `exterior_int`='%d', \
        `exterior_vw`='%d', \
        `teleport_x`='%f', \
        `teleport_y`='%f', \
        `teleport_z`='%f', \
        `teleport_vw`='%d', \
        `teleport_int`='%d' WHERE `id`='%d'",
        PickupsInfo[i][epdtDescription],
        PickupsInfo[i][epdtText],
        PickupsInfo[i][epdtModel], 
        PickupsInfo[i][epdtPossition_x], 
        PickupsInfo[i][epdtPossition_y],
        PickupsInfo[i][epdtPossition_z],
        PickupsInfo[i][epdtInt], 
        PickupsInfo[i][epdtVW],
        PickupsInfo[i][epdtInterior_possition_x], 
        PickupsInfo[i][epdtInterior_possition_y],
        PickupsInfo[i][epdtInterior_possition_z],
        PickupsInfo[i][epdtInterior_epdtVW], 
        PickupsInfo[i][epdtInterior_Int],
        PickupsInfo[i][epdtSQLID]);
    mysql_tquery(MySQL, query);
}

stock StreamerUpdatePickups(i)
{
    if(IsValidDynamic3DTextLabel(PickupsInfo[i][epdtText3D]))
        DestroyDynamic3DTextLabel(PickupsInfo[i][epdtText3D]);
    //if(IsValidDynamicPickup(PickupsInfo[i][epdtPickup]))
        DestroyDynamicPickup(PickupsInfo[i][epdtPickup]);

    if (!Iter_Contains(PickupIter, i))
        return true;

    PickupsInfo[i][epdtPickup] = CreateDynamicPickup(PickupsInfo[i][epdtModel], 0, PickupsInfo[i][epdtPossition_x], PickupsInfo[i][epdtPossition_y], PickupsInfo[i][epdtPossition_z], PickupsInfo[i][epdtVW], PickupsInfo[i][epdtInt]);

    if(strlen(PickupsInfo[i][epdtText]) > 0)
        PickupsInfo[i][epdtText3D] = CreateDynamic3DTextLabel(PickupsInfo[i][epdtText], -1, PickupsInfo[i][epdtPossition_x], PickupsInfo[i][epdtPossition_y], PickupsInfo[i][epdtPossition_z], 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, PickupsInfo[i][epdtVW], PickupsInfo[i][epdtInt]);
    return true;
}

stock GetClosestPickup(playerid, Float:range)
{
    foreach(new i : PickupIter)
        if (IsPlayerInRangeOfPoint(playerid, range, PickupsInfo[i][epdtPossition_x], PickupsInfo[i][epdtPossition_y], PickupsInfo[i][epdtPossition_z]) && PickupsInfo[i][epdtInt] == GetPlayerInterior(playerid) && PickupsInfo[i][epdtVW] == GetPlayerVirtualWorld(playerid))
            return i;
    return INVALID_PICKUP_ID;
}

stock GetPlayerPickupExit(playerid)
{
    foreach(new i : PickupIter)
        if(IsPlayerInRangeOfPoint(playerid, 1.0, PickupsInfo[i][epdtInterior_possition_x], PickupsInfo[i][epdtInterior_possition_y], PickupsInfo[i][epdtInterior_possition_z]) && GetPlayerInterior(playerid) == PickupsInfo[i][epdtInterior_Int] && (GetPlayerVirtualWorld(playerid) == PickupsInfo[i][epdtInterior_epdtVW] || GetPlayerVirtualWorld(playerid) == PickupsInfo[i][epdtSQLID]+200))
                return i;
    return INVALID_PICKUP_ID;
}