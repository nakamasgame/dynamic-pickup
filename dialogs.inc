#if defined _pickup_dialogs_included
	#endinput
#else
	#define _pickup_dialogs_included
#endif

enum
{
    DIALOG_PICKUP_MAIN = 500,
    DIALOG_PICKUP_MANAGEMENT,
    DIALOG_PICKUP_EDIT,
    DIALOG_PICKUP_MODEL,
    DIALOG_PICKUP_LIST,
    DIALOG_PICKUP_DESCRIPTION,
    DIALOG_PICKUP_TEXT_MAIN,
    DIALOG_PICKUP_TEXT_EDIT,
    DIALOG_PICKUP_POSSITION,
    DIALOG_PICKUP_POSSITION_EDIT,
    DIALOG_PICKUP_DELETE,
    DIALOG_PICKUP_TELEPORT_MAIN,
    DIALOG_PICKUP_TELEPORT_POSS,
    DIALOG_PICKUP_TELEPORT_INTERIOR,
    DIALOG_PICKUP_TELEPORT_VW
};


hook OnDialogResponse( playerid, dialogid, response, listitem, inputtext[ ] )
{
    switch(dialogid)
    {
        case DIALOG_PICKUP_MAIN:
        {
            if(!response)
                return 1;
            switch(listitem)
            {
                case 0: {
                    new id = Iter_Alloc(PickupIter);
                    ResetPickup(id);
                    GetPlayerPos(playerid, PickupsInfo[id][epdtPossition_x], PickupsInfo[id][epdtPossition_y], PickupsInfo[id][epdtPossition_z]);
                    PickupsInfo[id][epdtInt] = GetPlayerInterior(playerid);
                    PickupsInfo[id][epdtVW] = GetPlayerVirtualWorld(playerid);

                    SetPVarInt(playerid, "PickupIDManaged", id);
                    DialogPickup(playerid);
                }
                case 1: {
                    new id = GetClosestPickup(playerid, 2.0);
                    if(id != INVALID_PICKUP_ID)
                    {
                        SetPVarInt(playerid, "PickupIDManaged", id);
                        return DialogPickup(playerid);
                    }
                    return ShowPlayerDialog(playerid, DIALOG_PICKUP_EDIT, DIALOG_STYLE_INPUT, "{ffffff}Modification d'un pickup", "Veillez saisir l'id du pickup, que vous voulez modifier.", "Suivant", "Quitter");
                }
                case 2: ShowPlayerDialog(playerid, DIALOG_PICKUP_DELETE, DIALOG_STYLE_INPUT, "{ffffff}Suppression d'un pickup", "Veillez saisir l'id du pickup, que vous voulez supprimer.", "Supprimer", "Retour");
                case 3: ListPickupDialog(playerid);
                case 4: {
                    new pickupID = GetClosestPickup(playerid, 3.0),
                        str[100];
                    if(pickupID == INVALID_PICKUP_ID)
                        return SendClientMessage(playerid, -1, "{d74d4d}Aucun pickup n'est proche de vous.");
                    format(str, sizeof str, "{a3d8dc}Le pickup %s (id: %d) est le pickup le plus proche de vous.", PickupsInfo[pickupID][epdtDescription], pickupID);
                    SendClientMessage(playerid, -1, str);
                }
            }
        }
        case DIALOG_PICKUP_MANAGEMENT:
        {
            if(!response)
            {
                if(PickupsInfo[GetPVarInt(playerid, "PickupIDManaged")][epdtSQLID] == 0)
                {
                    Iter_Remove(PickupIter, GetPVarInt(playerid, "PickupIDManaged"));
                    StreamerUpdatePickups(GetPVarInt(playerid, "PickupIDManaged"));
                    ///Streamer_Update(playerid);
                }
                else ReloadPickup(GetPVarInt(playerid, "PickupIDManaged"));
                DeletePVar(playerid, "PickupIDManaged");
                return MainDialogPickup(playerid);
            }
            switch(listitem)
            {
                case 0: DescriptionPickupDialog(playerid);
                case 1: TextPickupDialog(playerid);
                case 2: ModelPickupDialog(playerid);
                case 3, 4: ShowPlayerDialog(playerid, DIALOG_PICKUP_POSSITION, DIALOG_STYLE_LIST, "{ffffff}Modification de la position du pickup", " » Déplacer à ma position\n » Déplacer à des coordonnées", "Suivant", "Quitter");
                case 5, 6: ShowPlayerDialog(playerid, DIALOG_PICKUP_TELEPORT_MAIN, DIALOG_STYLE_LIST, "{ffffff}Modification du point de téléportation", " » Choisir ma position actuelle pour la téléportation\n » Téléportation sur des coordonnées\n » Modifier l'intérieur.\n » Modifier le world de l'intérieur\n » Supprimer le point de téléportation", "Suivant", "Quitter");
                case 7: {
                    new pickupID = GetPVarInt(playerid, "PickupIDManaged");

                    if(PickupsInfo[pickupID][epdtSQLID] == 0)
                        InsertPickup(pickupID);
                    else UpdatePickup(pickupID);

                    MainDialogPickup(playerid);
                }
            }
        }
        case DIALOG_PICKUP_EDIT:
        {
            new pickupID;
            if(!response)
                return MainDialogPickup(playerid);
            if(sscanf(inputtext,"i", pickupID))
                return ShowPlayerDialog(playerid, DIALOG_PICKUP_EDIT, DIALOG_STYLE_INPUT, "{ffffff}Modification d'un pickup", "Veillez saisir l'id du pickup, que vous voulez modifier.", "Suivant", "Quitter");
            if (!Iter_Contains(PickupIter, pickupID))
                return ShowPlayerDialog(playerid, DIALOG_PICKUP_EDIT, DIALOG_STYLE_INPUT, "{ffffff}Modification d'un pickup", "Veillez saisir l'id du pickup, que vous voulez modifier.", "Suivant", "Quitter");
            SetPVarInt(playerid, "PickupIDManaged", pickupID);    
            DialogPickup(playerid);
        }
        case DIALOG_PICKUP_MODEL:
        {
            if(!response)
                return DialogPickup(playerid);

            PickupsInfo[GetPVarInt(playerid, "PickupIDManaged")][epdtModel] = PickupsModel[listitem][epdtModel];
            DialogPickup(playerid); 
        }
        case DIALOG_PICKUP_DESCRIPTION:
        {
            if(!response)
                return DialogPickup(playerid);
            if(strlen(inputtext) < 3)
                return DescriptionPickupDialog(playerid);
            format(PickupsInfo[GetPVarInt(playerid, "PickupIDManaged")][epdtDescription], 45, inputtext);
            return DialogPickup(playerid);
        }
        case DIALOG_PICKUP_LIST: MainDialogPickup(playerid);
        case DIALOG_PICKUP_TEXT_MAIN:
        {
            if(!response)
                return DialogPickup(playerid);
            switch(listitem)
            {
                case 0: TextEditPickupDialog(playerid);
                case 1: {
                    format(PickupsInfo[GetPVarInt(playerid, "PickupIDManaged")][epdtText], 45, "");
                    DialogPickup(playerid);
                }
            }
        }
        case DIALOG_PICKUP_TEXT_EDIT:
        {
            if(!response)
                return DialogPickup(playerid);
            if(strlen(inputtext) < 10)
                return TextEditPickupDialog(playerid);
            strcpy(PickupsInfo[GetPVarInt(playerid, "PickupIDManaged")][epdtText], inputtext, 100);
            DialogPickup(playerid);
        }
        case DIALOG_PICKUP_POSSITION:
        {
            if(!response)
                return MainDialogPickup(playerid);
            switch(listitem)
            {
                case 0:{
                    new pickupID = GetPVarInt(playerid, "PickupIDManaged");
                    GetPlayerPos(playerid, PickupsInfo[pickupID][epdtPossition_x], PickupsInfo[pickupID][epdtPossition_y], PickupsInfo[pickupID][epdtPossition_z]);
                    PickupsInfo[pickupID][epdtInt] = GetPlayerInterior(playerid);
                    PickupsInfo[pickupID][epdtVW] = GetPlayerVirtualWorld(playerid);
                    DialogPickup(playerid);
                }
                case 1: ShowPlayerDialog(playerid, DIALOG_PICKUP_POSSITION_EDIT, DIALOG_STYLE_INPUT, "{ffffff}Modification de la position du pickup", "Veuillez saisir ci-dessous les coordonnés GPS du pickup.\n\nExemple: x y z intérieur virtualWorld", "Modifier", "Retour");
            }
        }
        case DIALOG_PICKUP_POSSITION_EDIT:
        {
            new Float:possition_x, Float:possition_y, Float:possition_z, interior, virtualWorld, pickupID = GetPVarInt(playerid, "PickupIDManaged");
            if(!response)
                return DialogPickup(playerid);
            if(sscanf(inputtext,"fffdd", possition_x, possition_y, possition_z, interior, virtualWorld))
                return ShowPlayerDialog(playerid, DIALOG_PICKUP_POSSITION_EDIT, DIALOG_STYLE_INPUT, "{ffffff}Modification de la position du pickup", "Veuillez saisir ci-dessous les coordonnés GPS du pickup.\n\nExemple: x y z intérieur virtualWorld", "Modifier", "Retour");
            PickupsInfo[pickupID][epdtPossition_x] = possition_x;
            PickupsInfo[pickupID][epdtPossition_y] = possition_y;
            PickupsInfo[pickupID][epdtPossition_z] = possition_z;
            PickupsInfo[pickupID][epdtInt] = interior;
            PickupsInfo[pickupID][epdtVW] = virtualWorld;
            DialogPickup(playerid);
        }
        case DIALOG_PICKUP_DELETE:
        {
            new pickupID;
            if(!response)
                return MainDialogPickup(playerid);
            if(sscanf(inputtext,"i", pickupID))
                return ShowPlayerDialog(playerid, DIALOG_PICKUP_DELETE, DIALOG_STYLE_INPUT, "{ffffff}Suppression d'un pickup", "Veillez saisir l'id du pickup, que vous voulez supprimer.", "Supprimer", "Retour");
            if (!Iter_Contains(PickupIter, pickupID) || PickupsInfo[pickupID][epdtSQLID] == 0)
                return ShowPlayerDialog(playerid, DIALOG_PICKUP_DELETE, DIALOG_STYLE_INPUT, "{ffffff}Suppression d'un pickup", "Veillez saisir l'id du pickup, que vous voulez supprimer.", "Supprimer", "Retour");
            DeletePickup(pickupID);
            SendClientMessage(playerid, 0xFF0000AA, "Vous avez supprimé un pickup.");
            MainDialogPickup(playerid);
        }
        case DIALOG_PICKUP_TELEPORT_MAIN:
        {
            if(!response)
                return DialogPickup(playerid);  
            switch(listitem)
            {
                case 0: {
                    new pickupID = GetPVarInt(playerid, "PickupIDManaged");
                    GetPlayerPos(playerid, PickupsInfo[pickupID][epdtInterior_possition_x], PickupsInfo[pickupID][epdtInterior_possition_y], PickupsInfo[pickupID][epdtInterior_possition_z]);
                    PickupsInfo[pickupID][epdtInterior_epdtVW] = GetPlayerVirtualWorld(playerid);
                    PickupsInfo[pickupID][epdtInterior_Int] = GetPlayerInterior(playerid);
                    DialogPickup(playerid);
                }
                case 1: ShowPlayerDialog(playerid, DIALOG_PICKUP_TELEPORT_POSS, DIALOG_STYLE_INPUT, "{ffffff}Modification d'un pickup", "Veuillez saisir les coordonnés GPS du point de téléportation. (x y z)", "Modifier", "Retour");
                case 2: ShowPlayerDialog(playerid, DIALOG_PICKUP_TELEPORT_INTERIOR, DIALOG_STYLE_INPUT, "{ffffff}Modification d'un pickup", "Veillez saisir l'id de l'intérieur voulu.", "Modifier", "Retour");
                case 3: ShowPlayerDialog(playerid, DIALOG_PICKUP_TELEPORT_VW, DIALOG_STYLE_INPUT, "{ffffff}Modification d'un pickup", "Veillez saisir l'id du virtual world souhaiter. \n\nSi vous voulez une attribution automatique d'un world veillez  saisir  0.", "Modifier", "Retour");
                case 4:
                {
                    new pickupID = GetPVarInt(playerid, "PickupIDManaged");
                    PickupsInfo[pickupID][epdtInterior_possition_x] = 0.0;
                    PickupsInfo[pickupID][epdtInterior_possition_y] = 0.0;
                    PickupsInfo[pickupID][epdtInterior_possition_z] = 0.0;
                    DialogPickup(playerid);
                }
            }
        }
        case DIALOG_PICKUP_TELEPORT_POSS:
        {
            if(!response)
                return DialogPickup(playerid);  
            new Float:possition_x, Float:possition_y, Float:possition_z, pickupID = GetPVarInt(playerid, "PickupIDManaged");
            if(!response)
                return DialogPickup(playerid);
            if(sscanf(inputtext,"fff", possition_x, possition_y, possition_z))
                return ShowPlayerDialog(playerid, DIALOG_PICKUP_TELEPORT_POSS, DIALOG_STYLE_INPUT, "{ffffff}Modification d'un pickup", "Veuillez saisir les coordonnés GPS du point de téléportation. (x y z)", "Modifier", "Retour");
            PickupsInfo[pickupID][epdtInterior_possition_x] = possition_x;
            PickupsInfo[pickupID][epdtInterior_possition_y] = possition_y;
            PickupsInfo[pickupID][epdtInterior_possition_z] = possition_z;
            DialogPickup(playerid); 
        }
        case DIALOG_PICKUP_TELEPORT_INTERIOR:
        {
            new interior;
            if(!response)
                return DialogPickup(playerid); 
            if(sscanf(inputtext,"d", interior))
                return ShowPlayerDialog(playerid, DIALOG_PICKUP_TELEPORT_INTERIOR, DIALOG_STYLE_INPUT, "{ffffff}Modification d'un pickup", "Veillez saisir l'id de l'intérieur voulu.", "Modifier", "Retour");
            PickupsInfo[GetPVarInt(playerid, "PickupIDManaged")][epdtInterior_Int] = interior;
            DialogPickup(playerid); 
        }
        case DIALOG_PICKUP_TELEPORT_VW:
        {
            new virtualWorld;
            if(!response)
                return DialogPickup(playerid); 
            if(sscanf(inputtext,"d", virtualWorld))
                return ShowPlayerDialog(playerid, DIALOG_PICKUP_TELEPORT_VW, DIALOG_STYLE_INPUT, "{ffffff}Modification d'un pickup", "Veillez saisir l'id du virtual world souhaiter. \n\nSi vous voulez une attribution automatique d'un world veillez  saisir  0.", "Modifier", "Retour");
            PickupsInfo[GetPVarInt(playerid, "PickupIDManaged")][epdtInterior_epdtVW] = virtualWorld;
            DialogPickup(playerid); 
        }
    }
    return 1;
}
stock MainDialogPickup(playerid)
{
    return ShowPlayerDialog(playerid, DIALOG_PICKUP_MAIN, DIALOG_STYLE_LIST, "{ffffff}Gestion des pickups", " » Créer un pickup\n » Modifier un pickup\n » Supprimer un pickup\n » Liste des pickups\n » Obtenir l'ID du pickup le plus proche ", "Suivant", "Quitter");
} 

stock DialogPickup(playerid)
{
    new str[500], strTitle[50], id = GetPVarInt(playerid, "PickupIDManaged");
    
    if(PickupsInfo[id][epdtSQLID] == 0)
         format(strTitle, sizeof strTitle, "Création d'un pickup");
    else format(strTitle, sizeof strTitle, "Modification d'un pickup");

    if(strlen(PickupsInfo[id][epdtDescription]) > 0)
         format(str, sizeof str, "Description\t%s",PickupsInfo[id][epdtDescription]);
    else format(str, sizeof str, "Description\tAucune");

    if(strlen(PickupsInfo[id][epdtText]) == 0)
        format(str, sizeof str, "%s\nTexte en 3D\tAucun", str);
    else
        format(str, sizeof str, "%s\nTexte en 3D\tCliquez pour modifier", str);

    format(str, sizeof str, "%s\nModéle du pickup\t%d", str, PickupsInfo[id][epdtModel]);
    
    format(str, sizeof str, "%s\nPosition initial\t%f | %f | %f", str, PickupsInfo[id][epdtPossition_x], PickupsInfo[id][epdtPossition_y], PickupsInfo[id][epdtPossition_z]);
    format(str, sizeof str, "%s\n \tInterieur: %d | World: %d", str, PickupsInfo[id][epdtInt], PickupsInfo[id][epdtVW]);

    format(str, sizeof str, "%s\nPosition de la téléportation\t%f | %f | %f", str, PickupsInfo[id][epdtInterior_possition_x], PickupsInfo[id][epdtInterior_possition_y], PickupsInfo[id][epdtInterior_possition_z]);
    
    if(PickupsInfo[id][epdtInterior_epdtVW] == 0)
         format(str, sizeof str, "%s\n \tInterieur: %d | World: Automatique", str, PickupsInfo[id][epdtInterior_Int]);
    else format(str, sizeof str, "%s\n \tInterieur: %d | World: %d", str, PickupsInfo[id][epdtInterior_Int], PickupsInfo[id][epdtInterior_epdtVW]);

    if(PickupsInfo[id][epdtSQLID] == 0)
         format(str, sizeof str, "%s\nCréation du pickup", str);
    else format(str, sizeof str, "%s\nSauvegarder modification", str);

    StreamerUpdatePickups(id);
    Streamer_Update(playerid, STREAMER_TYPE_3D_TEXT_LABEL);
    return ShowPlayerDialog(playerid, DIALOG_PICKUP_MANAGEMENT, DIALOG_STYLE_TABLIST, strTitle, str, "Suivant", "Annuler");
}

stock ModelPickupDialog(playerid)
{
    new str[50*sizeof(PickupsModel)],
        pickupID = GetPVarInt(playerid, "PickupIDManaged");

    for(new i=0; i < sizeof PickupsModel; ++i)
        if(PickupsInfo[pickupID][epdtModel] ==  PickupsModel[i][epdtModel])
             format(str, sizeof str, "%s{00AF00}%s\t%d\n", str, PickupsModel[i][epdtDescription], PickupsModel[i][epdtModel]);
        else format(str, sizeof str, "%s{FF2800}%s\t%d\n", str, PickupsModel[i][epdtDescription], PickupsModel[i][epdtModel]);
    return ShowPlayerDialog(playerid, DIALOG_PICKUP_MODEL, DIALOG_STYLE_TABLIST, "Veillez choisir l'apparence de votre pickup", str, "Choisir", "Retour");
}

stock ListPickupDialog(playerid)
{
    new str[20*Iter_Size(PickupIter)];
    format(str, sizeof str, "ID\tDescription\tCréateur");

    foreach (new i : PickupIter)
    {
        if(strlen(PickupsInfo[i][epdtDescription]) > 0 && PickupsInfo[i][epdtSQLID] != 0)
            format(str, sizeof str, "%s\n%d\t%s\t%s", str, i, PickupsInfo[i][epdtDescription], PickupsInfo[i][epdtCreatorName]);
        else if(PickupsInfo[i][epdtSQLID] != 0)
            format(str, sizeof str, "%s\n%d\tAucune\t%s", str, i, PickupsInfo[i][epdtCreatorName]);
    }

    return ShowPlayerDialog(playerid, DIALOG_PICKUP_LIST, DIALOG_STYLE_TABLIST_HEADERS, "Liste des pickups", str, "OK", "");
}

stock DescriptionPickupDialog(playerid)
{
    return ShowPlayerDialog(playerid, DIALOG_PICKUP_DESCRIPTION, DIALOG_STYLE_INPUT, "{ffffff}Description du pickup", "Veillez saisir une courte description pour le pickup: \n\nExemple: Porte d'entrée du PDP de DT.", "Suivant", "Quitter");
}

stock TextEditPickupDialog(playerid)
{
    return ShowPlayerDialog(playerid, DIALOG_PICKUP_TEXT_EDIT, DIALOG_STYLE_INPUT, "{ffffff}Description du pickup", "Veuillez saisir le texte que vous souhaitez voir s'afficher.\nLégende:\n\t - \\n Pour un retour a la ligne.", "Modifier", "Retour");
}

stock TextPickupDialog(playerid)
{
    return ShowPlayerDialog(playerid, DIALOG_PICKUP_TEXT_MAIN, DIALOG_STYLE_LIST, "{ffffff}Modification du texte en 3D", "Modifier le texte\nSupprimer le texte", "Suivant", "Quitter");
}