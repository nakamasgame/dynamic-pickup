#if defined _pickup_core_included
	#endinput
#else
	#define _pickup_core_included
#endif

#include <YSI\y_hooks>

#include "pickup/constants"
#include "pickup/variables"
#include "pickup/functions"
#include "pickup/cmds/admin"
#include "pickup/dialogs"

hook OnGameModeInit()
{
    mysql_tquery(MySQL, "SELECT p.*, a.username  FROM gtrp_pickups p INNER JOIN gtrp_accounts a ON p.admin_accounts_id = a.id", "OnPickupsLoad");
	return true;
}

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
    if(newkeys == KEY_SECONDARY_ATTACK)
    {
        new pickupID = GetClosestPickup(playerid, 1.0);
        if(pickupID != INVALID_PICKUP_ID && PickupsInfo[pickupID][epdtInterior_possition_x] != 0.0)
        {
            SetPlayerPos(playerid, PickupsInfo[pickupID][epdtInterior_possition_x], PickupsInfo[pickupID][epdtInterior_possition_y], PickupsInfo[pickupID][epdtInterior_possition_z]);
            SetPlayerInterior(playerid, PickupsInfo[pickupID][epdtInterior_Int]);
            if(PickupsInfo[pickupID][epdtInterior_epdtVW] == 0)
                 SetPlayerVirtualWorld(playerid, PickupsInfo[pickupID][epdtSQLID]+200);
            else SetPlayerVirtualWorld(playerid, PickupsInfo[pickupID][epdtInterior_epdtVW]);
            return 1;
        }

        pickupID = GetPlayerPickupExit(playerid);
 
        if(pickupID != INVALID_PICKUP_ID)
        {
            SetPlayerPos(playerid, PickupsInfo[pickupID][epdtPossition_x], PickupsInfo[pickupID][epdtPossition_y], PickupsInfo[pickupID][epdtPossition_z]);
            SetPlayerInterior(playerid, PickupsInfo[pickupID][epdtInt]);
            SetPlayerVirtualWorld(playerid, PickupsInfo[pickupID][epdtVW]);
            return 1;
        }
        return 1;
    }
	return 1;
}

hook OnGameModeExit()
{
    for(new i=0; i < MAX_DB_PICKUPS; ++i) 
    {
        DestroyDynamic3DTextLabel(PickupsInfo[i][epdtText3D]);
        DestroyDynamicPickup(PickupsInfo[i][epdtPickup]);
    }
	return true;
}